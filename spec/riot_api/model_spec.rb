require "spec_helper"
require "riot_api"

include RiotApi

describe Model do
  describe "#inspect" do
    subject { Champion.new "id" => 212 }
    it "skips @raw" do
      expect(subject.inspect).not_to match(/@raw/)
    end
  end
end
