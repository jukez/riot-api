require "spec_helper"
require "riot_api"

include RiotApi

describe RuneSlot do
  it_behaves_like 'RiotApi model' do
    let(:valid_attributes) { { id: 1 } }
  end

  it_behaves_like "plain attribute" do
    let(:attribute) { "id" }
    let(:attribute_value) { "asd" }
  end

end
